package com.back01.api.controlador;

import com.back01.api.modelos.Ubicacion;
import com.back01.api.servicios.ServicioUbicacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas.UBICACION)
public class UbicacionController {

    @Autowired
    ServicioUbicacion servicioUbicacion;

    @GetMapping
    public List<Ubicacion> obtenerCuentas(){ return this.servicioUbicacion.obtenerUbicaciones();}

    // http://localhost:8090/api/v1/ubicaciones + DATOS
    @PostMapping
    public void agregarUbicacion(@RequestBody Ubicacion ubicacion){this.servicioUbicacion.insertarUbicacion(ubicacion);}

    // http://localhost:8090/api/v1/ubicaciones/{ubigueo}
    @GetMapping("/{ubigueo}")
    public Ubicacion obtenerunaUbicacion(@PathVariable String ubigueo) {
        System.out.println("obtenerunaUbicacion: "+ubigueo);
        return this.servicioUbicacion.obtenerUbicacion(ubigueo);
    }

    // http://localhost:8090/api/v1/cuentas/{documento} + DATOS
    @PutMapping("/{ubigueo}")
    public void reemplazarunaUbicacion(@PathVariable("ubigueo") String ubigueo,
                                    @RequestBody Ubicacion ubicacion){
        ubicacion.ubigueo = Integer.valueOf(ubigueo);
        System.out.println("obtenerunaUbicacion: "+ubigueo+ " + ubicacion.ubigueo :"+ ubicacion.ubigueo );
        this.servicioUbicacion.guardarUbicacion(ubicacion);

    }


    // DELETE http://localhost:9000/api/v1/cuentas/{ubigueo}
    @DeleteMapping("/{ubigueo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaUbicacion(@PathVariable String ubigueo) {
        try{
            this.servicioUbicacion.borrarUbicacion(ubigueo);
        } catch (Exception x) {
        }
    }
}
