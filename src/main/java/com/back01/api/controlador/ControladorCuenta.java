package com.back01.api.controlador;


import com.back01.api.modelos.Cuenta;
import com.back01.api.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)

public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    // http://localhost:8090/api/v1/cuentas
    @GetMapping
    public List<Cuenta> obtenerCuentas(){ return this.servicioCuenta.obtenerCuentas();}

    // http://localhost:8090/api/v1/cuentas + DATOS
    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta){this.servicioCuenta.insertarCuentaNueva(cuenta);}

    // http://localhost:8090/api/v1/cuentas/{documento}
    @GetMapping("/{numero}")
    public Cuenta obtenerunaCuenta(@PathVariable String numero) {
        System.out.println("obtenerunaCuenta: "+numero);
        return this.servicioCuenta.obtenerCuenta(numero);
    }

    // http://localhost:8090/api/v1/cuentas/{documento} + DATOS
    @PutMapping("/{numero}")
    public void reemplazarunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta){
        cuenta.numero = ctanumero;
        this.servicioCuenta.guardarCuenta(cuenta);

    }

    // http://localhost:8090/api/v1/cuentas/{documento} + DATOS
    @PatchMapping("/{numero}")
    public void emparcharunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta) {
        cuenta.numero = ctanumero;
        this.servicioCuenta.emparcharCuenta(cuenta);
    }

    // DELETE http://localhost:9000/api/v1/cuentas/{numero}
    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try{
            this.servicioCuenta.borrarCuenta(numero);
        } catch (Exception x) {
        }
    }

}
