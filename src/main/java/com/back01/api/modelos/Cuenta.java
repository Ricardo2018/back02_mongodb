package com.back01.api.modelos;

public class Cuenta {
    public String numero;
    public String moneda;
    public Double saldo;
    public String tipo;
    public String estado;
    public String oficina;
}
