package com.back01.api.repositorios;

import com.back01.api.modelos.Cuenta;
import com.back01.api.modelos.Ubicacion;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCuenta extends MongoRepository<Cuenta,String> {

}
