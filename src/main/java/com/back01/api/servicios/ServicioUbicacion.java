package com.back01.api.servicios;

import com.back01.api.modelos.Ubicacion;

import java.util.List;

public interface ServicioUbicacion {

    public List<Ubicacion> obtenerUbicaciones();

    //CREAR
    public void insertarUbicacion(Ubicacion ubicacion);

    //READ
    public Ubicacion obtenerUbicacion(String ubigueo);

    //UPDATE
    public void guardarUbicacion(Ubicacion ubicacion);

    //DELETE
    public  void borrarUbicacion(String ubigueo);
}
