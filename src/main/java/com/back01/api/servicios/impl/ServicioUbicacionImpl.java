package com.back01.api.servicios.impl;
import com.back01.api.modelos.Ubicacion;
import com.back01.api.repositorios.RepositorioUbicacion;
import com.back01.api.servicios.ServicioUbicacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioUbicacionImpl implements ServicioUbicacion{

    @Autowired
    RepositorioUbicacion repositorioUbicacion;

    @Override
    public List<Ubicacion> obtenerUbicaciones() {
        return this.repositorioUbicacion.findAll();
    }

    @Override
    public void insertarUbicacion(Ubicacion ubicacion) {
        this.repositorioUbicacion.insert(ubicacion);
    }

    @Override
    public Ubicacion obtenerUbicacion(String ubigueo) {
        final Optional<Ubicacion> quiUbicacion=this.repositorioUbicacion.findById(ubigueo);
        return quiUbicacion.isPresent()?quiUbicacion.get():null;
    }

    @Override
    public void guardarUbicacion(Ubicacion ubicacion) {
        System.out.println("guardarUbicacion: "+ubicacion.ubigueo.toString());
        this.repositorioUbicacion.save(ubicacion);
    }

    @Override
    public void borrarUbicacion(String ubigueo) {
        this.repositorioUbicacion.deleteById(ubigueo);
    }
}
