package com.back01.api.servicios.impl;

import com.back01.api.modelos.Cuenta;
import com.back01.api.modelos.Ubicacion;
import com.back01.api.repositorios.RepositorioCuenta;
import com.back01.api.repositorios.RepositorioUbicacion;
import com.back01.api.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    @Autowired
    RepositorioCuenta repositorioCuenta;


    @Override
    public List<Cuenta> obtenerCuentas() {
        return repositorioCuenta.findAll();
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.repositorioCuenta.insert(cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        final Optional<Cuenta> quiCuenta=this.repositorioCuenta.findById(numero);
        return quiCuenta.isPresent()?quiCuenta.get():null;
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        this.repositorioCuenta.save(cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {

    }

    @Override
    public void borrarCuenta(String numero) {
        this.repositorioCuenta.deleteById(numero);
    }
}
